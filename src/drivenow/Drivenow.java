package drivenow;

import drivenow.boundaries.FahrlehrerinBoundary;

import drivenow.boundaries.SekretaerBoundary;
import drivenow.interfaces.IDrivenow;
import drivenow.interfaces.IFahrlehrerinBoundary;
import drivenow.interfaces.ISekretaerBoundary;

public class Drivenow implements IDrivenow 
{

	private IFahrlehrerinBoundary fahrlehrerinBoundary;
	private ISekretaerBoundary sekretaerBoundary;

	@Override
	public IFahrlehrerinBoundary getFahrlehrerinBoundary() {
		return fahrlehrerinBoundary;
	}

	@Override
	public ISekretaerBoundary getSekretaerBoundary() {
		return sekretaerBoundary;
	}

	@Override
	public void initialisieren()
	{
		fahrlehrerinBoundary = new FahrlehrerinBoundary();
		sekretaerBoundary = new SekretaerBoundary();
	}
}
