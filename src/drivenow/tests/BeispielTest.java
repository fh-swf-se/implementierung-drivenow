package drivenow.tests;

import java.util.Calendar;

import drivenow.Drivenow;
import drivenow.Systemereignis;
import drivenow.Systemereignis.Nachricht;
import drivenow.interfaces.IDrivenow;
import drivenow.interfaces.IFahrlehrerinBoundary;
import drivenow.interfaces.ISekretaerBoundary;


public class BeispielTest
{
	public static void main(String[] args)
	{
		// Systeminstanz initialisieren
		IDrivenow driveNowSystem = new Drivenow(); 
		driveNowSystem.initialisieren();

		// Interfaces 
		IFahrlehrerinBoundary fahrlehrerinBoundary = driveNowSystem.getFahrlehrerinBoundary();
		ISekretaerBoundary sekretaerBoundary = driveNowSystem.getSekretaerBoundary();

		// Systemereignisse (Ausgabe)
		Systemereignis systemereignis;

		
		//////////////////////////////////////////////////////////////////////////
		System.out.println("Fahrschulautos hinzuf�gen");
		System.out.println("======================================================");
		
		System.out.println("Erwartet wird 3x : "+ Nachricht.Fahrschulauto_erfolgreich_hinzugefuegt);
		systemereignis = sekretaerBoundary.fahrschulautoEintragen("HA-FH-1111");
		System.out.println("Systemantwort ist: " + systemereignis);		
		//??????????????????????????????? Vorarbeit ???????????????????????????????	
		// F�gen Sie 2 weitere Fahrzeuge hinzu
		systemereignis = sekretaerBoundary.fahrschulautoEintragen("HA-FH-1112");
		System.out.println("Systemantwort ist: " + systemereignis);	
		
		systemereignis = sekretaerBoundary.fahrschulautoEintragen("HA-FH-1113");
		System.out.println("Systemantwort ist: " + systemereignis);	
		
		System.out.println();
		System.out.println();
		//??????????????????????????????? Vorarbeit ???????????????????????????????
		
		
		//??????????????????????????????? AUFGABE 2 ???????????????????????????????	
		// Gestalten Sie den Test-Code so, dass alle Szenarien der Systemoperation
		// "FahrlehrerinEintragen" mindestens einmal ausgef�hrt werden, 
		// d.h. jedes Systemereignis mindestens einmal zur�ckgegeben wird.
		System.out.println("Fahrlehrerin hinzuf�gen");
		System.out.println("=========================================");
		
		System.out.println("Erwartet wird    : " + Nachricht.Fahrlehrerin_nicht_hinzugefuegt_Auto_unbekannt);

		/** Nachricht 1 **/
		systemereignis = sekretaerBoundary.fahrlehrerinEintragen("M�ller", "HA-FH-1234");
		System.out.println("Systemantwort ist: " + systemereignis);	


		System.out.println("Erwartet wird    : " + Nachricht.Fahrlehrerin_nicht_hinzugefuegt_Auto_vergeben);

		/** Nachricht 2 **/
		sekretaerBoundary.fahrlehrerinEintragen("Schulz", "HA-FH-1111");
		systemereignis = sekretaerBoundary.fahrlehrerinEintragen("M�ller", "HA-FH-1111");
		System.out.println("Systemantwort ist: " + systemereignis);	
		
		System.out.println("Erwartet wird    : " + Nachricht.Fahrlehrerin_erfolgreich_hinzugefuegt);

		/** Nachricht 3 **/
		systemereignis = sekretaerBoundary.fahrlehrerinEintragen("M�ller", "HA-FH-1112");
		System.out.println("Systemantwort ist: " + systemereignis);	
		
		System.out.println();
		System.out.println();
		//??????????????????????????????? AUFGABE 2 ???????????????????????????????
		
		
		//////////////////////////////////////////////////////////////////////////
		System.out.println("Fahrschueler hinzuf�gen");
		System.out.println("======================================================");
		System.out.println("Erwartet wird    : " + Nachricht.Fahrschueler_erfolgreich_hinzugefuegt);
		systemereignis = sekretaerBoundary.fahrschuelerEintragen("Marco", "Am Strand 1, 58000 Hagen");		
		String schuelerID_Marco = systemereignis.getID();
		System.out.println("Systemantwort ist: " + systemereignis);
		System.out.println();
		System.out.println();
		//////////////////////////////////////////////////////////////////////////


		//////////////////////////////////////////////////////////////////////////
		System.out.println("Kalendereintraege anlegen und Theoriestunden eintragen");
		System.out.println("======================================================");
		Calendar beginn1, beginn2, beginn3, beginn4, beginn5, beginn6, beginn7;
		Calendar beginn8, beginn9, beginn10, beginn11, beginn12, beginn13, beginn13_1, beginn1_1;
		
		beginn1 = Calendar.getInstance();
		beginn1.set(2019, 9, 20, 15, 00);
		sekretaerBoundary.theoriestundeEintragen(1, beginn1.getTime());
		
		beginn2 = Calendar.getInstance();
		beginn2.set(2019, 9, 21, 15, 00);
		sekretaerBoundary.theoriestundeEintragen(2, beginn2.getTime());
		
		beginn3 = Calendar.getInstance();
		beginn3.set(2019, 9, 22, 15, 00);
		sekretaerBoundary.theoriestundeEintragen(3, beginn3.getTime());
		
		beginn4 = Calendar.getInstance();
		beginn4.set(2019, 9, 23, 15, 00);
		sekretaerBoundary.theoriestundeEintragen(4, beginn4.getTime());
		
		beginn5 = Calendar.getInstance();
		beginn5.set(2019, 9, 24, 15, 00);
		sekretaerBoundary.theoriestundeEintragen(5, beginn5.getTime());
		
		beginn6 = Calendar.getInstance();
		beginn6.set(2019, 9, 25, 15, 00);
		sekretaerBoundary.theoriestundeEintragen(6, beginn6.getTime());
		
		beginn7 = Calendar.getInstance();
		beginn7.set(2019, 9, 26, 15, 00);
		sekretaerBoundary.theoriestundeEintragen(7, beginn7.getTime());
		
		beginn8 = Calendar.getInstance();
		beginn8.set(2019, 9, 27, 15, 00);
		sekretaerBoundary.theoriestundeEintragen(8, beginn8.getTime());
		
		beginn9 = Calendar.getInstance();
		beginn9.set(2019, 9, 28, 15, 00);
		sekretaerBoundary.theoriestundeEintragen(9, beginn9.getTime());
		
		beginn10 = Calendar.getInstance();
		beginn10.set(2019, 9, 29, 15, 00);
		sekretaerBoundary.theoriestundeEintragen(10, beginn10.getTime());
		
		beginn11 = Calendar.getInstance();
		beginn11.set(2019, 9, 30, 15, 00);
		sekretaerBoundary.theoriestundeEintragen(11, beginn11.getTime());
		
		beginn12 = Calendar.getInstance();
		beginn12.set(2019, 9, 31, 15, 00);
		sekretaerBoundary.theoriestundeEintragen(12, beginn12.getTime());
		
		beginn1_1 = Calendar.getInstance();
		beginn1_1.set(2019, 10, 20, 15, 00);
		sekretaerBoundary.theoriestundeEintragen(1, beginn1_1.getTime());
		
		beginn13 = Calendar.getInstance();
		beginn13.set(2019, 10, 21, 15, 00);
		sekretaerBoundary.theoriestundeEintragen(13, beginn13.getTime());
		
		beginn13_1 = Calendar.getInstance();
		beginn13_1.set(2019, 10, 22, 15, 00);
		sekretaerBoundary.theoriestundeEintragen(13, beginn13_1.getTime());
		System.out.println();
		System.out.println();
		
		//////////////////////////////////////////////////////////////////////////
		System.out.println("Theoriestunde vermerken");
		System.out.println("======================================================");
		
		//??????????????????????????????? AUFGABE 3 ???????????????????????????????	
		// Gestalten Sie den Test-Code so, dass alle Szenarien der Systemoperation
		// "theoriestundeVermerken" mindestens einmal ausgef�hrt werden, 
		// d.h. jedes Systemereignis mindestens einmal zur�ckgegeben wird.

		/** Nachricht 1 **/
		System.out.println("Erwartet wird    : " + Nachricht.Theoriestunde_erfolgreich_vermerkt);
		systemereignis = sekretaerBoundary.theoriestundeVermerken(schuelerID_Marco, beginn1.getTime());
		System.out.println("Systemantwort ist: " + systemereignis);

		/** Nachricht 2 **/
		System.out.println("Erwartet wird    : " + Nachricht.Theoriestunde_nicht_vermerkt_bereits_vermerkt);
		// Nochmal vermerken
		systemereignis = sekretaerBoundary.theoriestundeVermerken(schuelerID_Marco, beginn1.getTime());
		System.out.println("Systemantwort ist: " + systemereignis);

		/** Nachricht 3 **/
		System.out.println("Erwartet wird    : " + Nachricht.Theoriestunde_nicht_vermerkt_bereits_genug_Grundlagen);
		// Eintragen der restlichen Grundlagen
		sekretaerBoundary.theoriestundeVermerken(schuelerID_Marco, beginn2.getTime());
		sekretaerBoundary.theoriestundeVermerken(schuelerID_Marco, beginn3.getTime());
		sekretaerBoundary.theoriestundeVermerken(schuelerID_Marco, beginn4.getTime());
		sekretaerBoundary.theoriestundeVermerken(schuelerID_Marco, beginn5.getTime());
		sekretaerBoundary.theoriestundeVermerken(schuelerID_Marco, beginn6.getTime());
		sekretaerBoundary.theoriestundeVermerken(schuelerID_Marco, beginn7.getTime());
		sekretaerBoundary.theoriestundeVermerken(schuelerID_Marco, beginn8.getTime());
		sekretaerBoundary.theoriestundeVermerken(schuelerID_Marco, beginn9.getTime());
		sekretaerBoundary.theoriestundeVermerken(schuelerID_Marco, beginn10.getTime());
		sekretaerBoundary.theoriestundeVermerken(schuelerID_Marco, beginn11.getTime());
		sekretaerBoundary.theoriestundeVermerken(schuelerID_Marco, beginn12.getTime());

		// versuche, Thema 1 an einem anderen Tag einzutragen
		systemereignis = sekretaerBoundary.theoriestundeVermerken(schuelerID_Marco, beginn1_1.getTime());
		System.out.println("Systemantwort ist: " + systemereignis);		
		
		/** Nachricht 4 **/
		System.out.println("Erwartet wird    : " + Nachricht.Theoriestunde_nicht_vermerkt_bereits_Sonderthema);
		// besuche Sonderthema 13
		sekretaerBoundary.theoriestundeVermerken(schuelerID_Marco, beginn13.getTime());
		
		// versuche, dass gleiche Sonderthema zu einem anderen Termin einzutragen
		systemereignis = sekretaerBoundary.theoriestundeVermerken(schuelerID_Marco, beginn13_1.getTime());
		System.out.println("Systemantwort ist: " + systemereignis);		

		
		//??????????????????????????????? AUFGABE 3 ???????????????????????????????
	
		System.out.println("Datenbestand von ISekretaerBoundary");
		System.out.println(sekretaerBoundary.datenbestandZurueckgeben());
		
		System.out.println("Datenbestand von IFahrlehrerinBoundary");
		System.out.println(fahrlehrerinBoundary.unterrichtsstundenZurueckgeben());
	}
}
