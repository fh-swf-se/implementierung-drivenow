package drivenow.entities;

public abstract class AbstractEntityWithID {

	protected String ID;

	public String getID() {
		return ID;
	}

	public abstract String toString();

}
