package drivenow.entities;

public class Auto extends AbstractEntityWithID
{
	private Fahrlehrerindaten fahrlehrerinDaten;

	public Auto(String kennzeichen)
	{
		this.ID = kennzeichen;
	}

	public Fahrlehrerindaten getFahrlehrerinDaten()
	{
		return fahrlehrerinDaten;
	}

	public void setFahrlehrerinDaten(Fahrlehrerindaten fahrlehrerinDaten)
	{
		this.fahrlehrerinDaten = fahrlehrerinDaten;
	}

	@Override
	public String toString()
	{
		String autoString = "Fahrschulauto " + this.ID;
		
		if (fahrlehrerinDaten != null)
		{
			autoString += " mit Fahrlehrerin " + fahrlehrerinDaten.getName();
		}
		else
		{
			autoString += " ohne Fahrlehrerin";
		}
		return autoString;
	}

}
