package drivenow.entities;

import java.util.Date;

public class Theoriestunde extends Unterricht {

	private int thema;

	public Theoriestunde(int thema, Date beginn) {
		this.thema = thema;
		this.beginn = beginn;
	}

	public int getThema()
	{
		return thema;
	}

	@Override
	public String toString()
	{
		String fahrstundenString = "Theoriestunde " + dateFormat.format(beginn)	+ " (" + thema + ")";
		return fahrstundenString;
	}

	public boolean istGrundlagenstunde()
	{
		return thema <= 12;
	}
}
