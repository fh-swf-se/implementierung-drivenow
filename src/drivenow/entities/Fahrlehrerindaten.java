package drivenow.entities;

import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;

public class Fahrlehrerindaten extends AbstractEntityWithID
{

	private String name;
	private static int instanceCounter = 0;

	private HashSet<Fahrstunde> fahrstunden;
	private HashSet<Theoriestunde> theoriestunden;

	private Auto auto;

	@SuppressWarnings("unused")
	private Fahrlehrerindaten()
	{
	}

	
	public Fahrlehrerindaten(String name)
	{
		this.ID = Integer.toString(instanceCounter++);
		this.name = name;
		fahrstunden = new HashSet<Fahrstunde>();
		theoriestunden = new HashSet<Theoriestunde>();
	}

	
	public String getID()
	{
		return ID;
	}

	
	public String getName()
	{
		return name;
	}

	
	public Auto getAuto()
	{
		return auto;
	}

	
	public boolean istFrei(Date beginn, int dauerInMinuten)
	{
		boolean istFrei = true;
		Calendar endZeitpunktNeu = Calendar.getInstance();
		endZeitpunktNeu.setTime(beginn);
		endZeitpunktNeu.add(Calendar.MINUTE, dauerInMinuten);
		Date anfangszeitpunktNeuDate = beginn;
		Date endzeitpunktNeuDate = endZeitpunktNeu.getTime();
		HashSet<Unterricht> unterricht = new HashSet<Unterricht>();
		unterricht.addAll(theoriestunden);
		unterricht.addAll(fahrstunden);
		Date anfangszeitpunktDate = null;
		Date endzeitpunktDate = null;
		boolean endetDavor = false;
		boolean beginntDanach = false;
		for (Unterricht eineUnterricht : unterricht)
		{
			Calendar endZeitpunkt = Calendar.getInstance();
			endZeitpunkt.setTime(eineUnterricht.getBeginn());
			if (eineUnterricht instanceof Theoriestunde)
			{
				endZeitpunkt.add(Calendar.MINUTE, 90);
			}
			else
			{
				endZeitpunkt.add(Calendar.MINUTE, 45);
			}
			anfangszeitpunktDate = eineUnterricht.getBeginn();
			endzeitpunktDate = endZeitpunkt.getTime();
			endetDavor = endzeitpunktDate.before(anfangszeitpunktNeuDate);
			beginntDanach = anfangszeitpunktDate.after(endzeitpunktNeuDate);
			istFrei &= endetDavor || beginntDanach;
			if (!istFrei)
			{
				break;
			}
		}
		return istFrei;
	}

	
	public void addTheoriestunde(Theoriestunde theorieStunde)
	{
		theoriestunden.add(theorieStunde);
	}

	
	public void addFahrstunde(Fahrstunde fahrstunde)
	{
		fahrstunden.add(fahrstunde);
	}

	
	public void setAuto(Auto auto)
	{
		this.auto = auto;
	}

	
	
	@Override
	public String toString()
	{
		String fahrlehrerinString = "Fahrlehrerin " + this.name + " [" + this.ID + "] \n \t Fahrstunden: ";
		
		if (!fahrstunden.isEmpty())
		{
			for (Fahrstunde fahrstunde : fahrstunden)
			{
				fahrlehrerinString += fahrstunde.toString() + " ";
			}
		}
		else
		{
			fahrlehrerinString += "keine";
		}
		
		fahrlehrerinString += "\n \t Theoriestunden: ";
		
		if (!theoriestunden.isEmpty())
		{
			for (Theoriestunde theoriestunde : theoriestunden)
			{
				fahrlehrerinString += theoriestunde.toString() + " ";
			}
		}
		else
		{
			fahrlehrerinString += "keine";
		}
		return fahrlehrerinString;
	}
}
