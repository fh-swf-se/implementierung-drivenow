package drivenow.entities;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Unterricht {

	protected Fahrlehrerindaten verantwortlicheFahrlehrerin;
	protected Date beginn;
	protected DateFormat dateFormat;

	public Unterricht() {
		this.dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm");
	}

	public Date getBeginn() {
		return beginn;
	}

}
