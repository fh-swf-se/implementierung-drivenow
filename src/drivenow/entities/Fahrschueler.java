package drivenow.entities;

import java.util.Date;
import java.util.HashSet;

public class Fahrschueler extends AbstractEntityWithID {

	private String name;
	private String anschrift;

	private static int instanceCounter = 0;

	private Fahrlehrerindaten fahrlehrerin;
	private HashSet<Theoriestunde> vermerkteTheoriestunden;
	private HashSet<Fahrstunde> fahrstunden;

	@SuppressWarnings("unused")
	private Fahrschueler() {
	}

	public Fahrschueler(String name, String anschrift)
	{
		this.ID = Integer.toString(instanceCounter++);
		this.name = name;
		this.anschrift = anschrift;
		vermerkteTheoriestunden = new HashSet<Theoriestunde>();
		fahrstunden = new HashSet<Fahrstunde>();
	}

	public Fahrlehrerindaten getFahrlehrerin() {
		return fahrlehrerin;
	}

	public void setFahrlehrerin(Fahrlehrerindaten fahrlehrerin) {
		this.fahrlehrerin = fahrlehrerin;
	}

	public void addFahrstunde(Fahrstunde fahrstunde) {
		this.fahrstunden.add(fahrstunde);
	}

	@Override
	public String toString() {
		String fahrschuelerString = "Fahrschueler " + this.name + ", "
				+ this.anschrift + " [" + this.ID + "] " + "\n\tLehrerin: "
				+ fahrlehrerin.getName() + "\n\tTheoriestunden: ";
		if (!vermerkteTheoriestunden.isEmpty()) {
			String theorieThemen = "(";
			for (Theoriestunde theoriestunde : vermerkteTheoriestunden) {
				theorieThemen += theoriestunde.getThema() + ",";
			}
			theorieThemen = theorieThemen.substring(0,
					theorieThemen.length() - 1) + ")";
			fahrschuelerString += theorieThemen;
		} else {
			fahrschuelerString += "keine";
		}
		fahrschuelerString += "\n\tFahrstunden: ";
		if (!fahrstunden.isEmpty()) {
			String fahrstundenString = "";
			for (Fahrstunde fahrstunde : fahrstunden) {
				fahrstundenString += fahrstunde.toString() + ",";
			}
			fahrstundenString = fahrstundenString.substring(0,
					fahrstundenString.length() - 1);
			fahrschuelerString += fahrstundenString;
		} else {
			fahrschuelerString += "keine";
		}
		return fahrschuelerString;
	}

	public boolean hatTheoriestundeBereitsBesucht(Theoriestunde theoriestunde) {
		return vermerkteTheoriestunden.contains(theoriestunde);
	}

	public boolean hatZwoelfGrundlagen() {
		boolean hatZwoelfGrundlagen = false;
		int grundlagenCounter = 0;
		for (Theoriestunde stunde : vermerkteTheoriestunden) {
			if (stunde.getThema() <= 12) {
				grundlagenCounter += 1;
			}
		}
		hatZwoelfGrundlagen = grundlagenCounter >= 12;
		return hatZwoelfGrundlagen;
	}

	
	public boolean hatSonderthemaBereits(int thema) {
		boolean hatSonderthemaBereits = false;
		for (Theoriestunde stunde : vermerkteTheoriestunden) {
			if (stunde.getThema() == thema) {
				hatSonderthemaBereits = true;
				break;
			}
		}
		return hatSonderthemaBereits;
	}

	public boolean vermerkeTheoriestunde(Theoriestunde stunde) {
		return vermerkteTheoriestunden.add(stunde);
	}

	public Fahrstunde getFahrstunde(Date beginn) {
		Fahrstunde fahrstunde = null;
		for (Fahrstunde eineStunde : fahrstunden) {
			if (eineStunde.getBeginn().compareTo(beginn) == 0) {
				fahrstunde = eineStunde;
				break;
			}
		}
		return fahrstunde;
	}
/*
	public boolean removeFahrstunde(Fahrstunde fahrstunde) {
		return fahrstunden.remove(fahrstunde);
	}

	public void theoriepruefungsbestehenEintragen() {
		this.theoriepruefungBestanden = true;
	}

	public boolean hatAutobahnstundenAbsolviert() {
		return zaehleFahrstundenNachTyp(Stundenart.Autobahnfahrt) >= 4;
	}

	public boolean hatUeberlandstundenAbsolviert() {
		return zaehleFahrstundenNachTyp(Stundenart.Ueberlandfahrt) >= 5;
	}

	public boolean hatBeleuchtungsstundenAbsolviert() {
		return zaehleFahrstundenNachTyp(Stundenart.Beleuchtungsfahrt) >= 3;
	}

	private int zaehleFahrstundenNachTyp(Stundenart typ) {
		int counter = 0;
		for (Fahrstunde fahrstunde : fahrstunden) {
			if (fahrstunde.getTyp() == typ) {
				counter += 1;
			}
		}
		return counter;
	}

	public boolean hatTheoriepruefungBestanden() {
		return theoriepruefungBestanden;
	}
*/
}
