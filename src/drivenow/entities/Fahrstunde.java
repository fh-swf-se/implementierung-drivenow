package drivenow.entities;

import java.util.Date;

import drivenow.interfaces.ISekretaerBoundary.Stundenart;

public class Fahrstunde extends Unterricht
{

	private Stundenart typ;

	public Fahrstunde(Stundenart art, Date beginn)
	{
		this.typ = art;
		this.beginn = beginn;
	}

	public Stundenart getTyp()
	{
		return typ;
	}

	@Override
	public String toString()
	{
		String fahrstundenString = "Fahrstunde " + dateFormat.format(beginn) + " (" + typ + ")";
		return fahrstundenString;
	}
}
