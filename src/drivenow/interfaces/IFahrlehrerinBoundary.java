package drivenow.interfaces;

/**
 * Interface fuer die FahrlehrerinBoundary.
 * 
 * Dieses Interface darf nicht veraendert werden.
 *
 */
public interface IFahrlehrerinBoundary
{
	/**
	 * Diese Methode gibt eine Nachricht ueber das Anzeigen der
	 * Unterrichtsstunden und eine Repraesentation des Datenbestandes aller
	 * Unterrichtsstunden als Text im Systemereignis zurueck. Sie dient dem
	 * Debugging und hilft, den Ueberblick zu behalten. Alle vorhandenen
	 * Unterrichtsstunden mit verantwortlicher Lehrerin, Datum und Art werden
	 * moeglichst leserlich aufbereitet. Sortiert nach Fahrlehrerin. Existieren
	 * keine Unterrichtsstunden wird der Text
	 * "Keine Unterrichtsstunden im System vorhanden" uebermittelt. Der Text
	 * wird zurueckgegeben und NICHT direkt ausgegeben.
	 * 
	 * PRE: true.
	 * 
	 * @return String mit Datenbestand als lesbar formatierter Text
	 */
	public String unterrichtsstundenZurueckgeben();

}
