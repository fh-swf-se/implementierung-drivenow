package drivenow.controls;

import java.util.HashMap;
import java.util.HashSet;

import drivenow.Systemereignis;
import drivenow.Systemereignis.Nachricht;
import drivenow.entities.Auto;
import drivenow.entities.Fahrlehrerindaten;
import drivenow.entities.Fahrschueler;

public class Ressourcenverwaltung
{

	private static Ressourcenverwaltung instance;

	private HashMap<String, Auto> fahrschulautos;
	private HashSet<Fahrlehrerindaten> fahrlehrerinnen;
	private HashMap<String, Fahrschueler> fahrschueler;

	private Unterrichtsverwaltung unterrichtsverwaltung;

	private Ressourcenverwaltung()
	{
		fahrschulautos = new HashMap<String, Auto>();
		fahrlehrerinnen = new HashSet<Fahrlehrerindaten>();
		fahrschueler = new HashMap<String, Fahrschueler>();
		unterrichtsverwaltung = Unterrichtsverwaltung.getInstance();
	}

	public static Ressourcenverwaltung getInstance()
	{
		if (instance == null) {
			instance = new Ressourcenverwaltung();
		}
		return instance;
	}

	public Systemereignis fahrschulautoEintragen(String kennzeichen) {
		Systemereignis systemereignis;
		Auto auto = fahrschulautos.get(kennzeichen);
		if (auto == null) {
			auto = new Auto(kennzeichen);
			fahrschulautos.put(kennzeichen, auto);
			systemereignis = new Systemereignis(
					Nachricht.Fahrschulauto_erfolgreich_hinzugefuegt);
		} else {
			systemereignis = new Systemereignis(
					Nachricht.Auto_mit_kennzeichen_bereits_vorhanden);
		}
		return systemereignis;
	}

	/**
	 * F�gt eine neuen Fahrlehrerin hinzu.
	 * 
	 * @param name Name der Fahrlehrerin
	 * @param kennzeichen Kennzeichen des Fahrzeuges
	 * 
	 * @return Systemereignis mit Systemnachricht
	 */
	public Systemereignis fahrlehrerinEintragen(String name, String kennzeichen)
	{
		Systemereignis systemereignis = null;
		Auto auto = fahrschulautos.get(kennzeichen);
		
		if (auto == null)
		{
			systemereignis = new Systemereignis(Nachricht.Fahrlehrerin_nicht_hinzugefuegt_Auto_unbekannt);
		}
		else
		{
			Fahrlehrerindaten fl = auto.getFahrlehrerinDaten();
			if (fl != null)
			{
				systemereignis = new Systemereignis(Nachricht.Fahrlehrerin_nicht_hinzugefuegt_Auto_vergeben);
			}
			else
			{
				fl = new Fahrlehrerindaten(name);
				fl.setAuto(auto);
				
				fahrlehrerinnen.add(fl);
				unterrichtsverwaltung.addFahrlehrerin(fl);
				
				auto.setFahrlehrerinDaten(fl);
				systemereignis = new Systemereignis(Nachricht.Fahrlehrerin_erfolgreich_hinzugefuegt);
			}
		}
		return systemereignis;
	}
	
	
	public Systemereignis fahrschuelerEintragen(String name, String anschrift)
	{
		Systemereignis systemereignis;
		if (fahrlehrerinnen.isEmpty()) {
			systemereignis = new Systemereignis(
					Nachricht.Fahrschueler_nicht_hinzugefuegt_keine_Fahrlehrerin_vorhanden);
		} else {
			Fahrschueler schueler = new Fahrschueler(name, anschrift);
			int randomNumber = (int) (fahrlehrerinnen.size() * Math.random());
			Fahrlehrerindaten fahrlehrerin = fahrlehrerinnen
					.toArray(new Fahrlehrerindaten[0])[randomNumber];
			schueler.setFahrlehrerin(fahrlehrerin);
			fahrschueler.put(schueler.getID(), schueler);
			unterrichtsverwaltung.addFahrschueler(schueler);
			//pruefungsverwaltung.addFahrschueler(schueler);
			systemereignis = new Systemereignis(Nachricht.Fahrschueler_erfolgreich_hinzugefuegt);
			systemereignis.setID(schueler.getID());
		}

		return systemereignis;
	}

	public String datenbestandZurueckgeben()
	{
		String datenbestand = "Keine Daten im System";
		if (!fahrlehrerinnen.isEmpty() ||/* !fahrschueler.isEmpty() ||*/ !fahrschulautos.isEmpty())
		{
			datenbestand = "";
			datenbestand = "Fahrschulautos: \n ==============\n";
			if (!fahrschulautos.isEmpty()) {
				for (Auto auto : fahrschulautos.values()) {
					datenbestand += auto.toString() + "\n";
				}
			}
			else
			{
				datenbestand += "keine\n";
			}
			datenbestand += "\r\n";
			datenbestand += "Fahrlehrerinnen: \n ==============\n";
			if (!fahrlehrerinnen.isEmpty()) {
				for (Fahrlehrerindaten fahrlehrerin : fahrlehrerinnen) {
					datenbestand += fahrlehrerin.toString() + "\n";
				}
			} else {
				datenbestand += "keine\n";
			}
			datenbestand += "\r\n";
			datenbestand += "Fahrschueler: \n ==============\n";
			if (!fahrschueler.isEmpty())
			{
				for (Fahrschueler einFahrschueler : fahrschueler.values())
				{
					datenbestand += einFahrschueler.toString() + "\n";
				}
			} 
			else
			{
				datenbestand += "keine\n";
			}
		}
		return datenbestand;
	}

	public void reset() {
		instance = null;
	}
}
