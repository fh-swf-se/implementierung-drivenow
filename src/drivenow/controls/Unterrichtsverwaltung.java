package drivenow.controls;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;

import drivenow.Systemereignis;
import drivenow.Systemereignis.Nachricht;
import drivenow.entities.Fahrlehrerindaten;
import drivenow.entities.Fahrschueler;
import drivenow.entities.Fahrstunde;
import drivenow.entities.Theoriestunde;

public class Unterrichtsverwaltung
{

	private static Unterrichtsverwaltung instance;

	private HashSet<Theoriestunde> theoriestunden;
	private HashSet<Fahrstunde> fahrstunden;
	private HashSet<Fahrlehrerindaten> fahrlehrerinnen;
	private HashMap<String, Fahrschueler> fahrschueler;

	private Unterrichtsverwaltung()
	{
		theoriestunden = new HashSet<Theoriestunde>();
		fahrstunden = new HashSet<Fahrstunde>();
		fahrlehrerinnen = new HashSet<Fahrlehrerindaten>();
		fahrschueler = new HashMap<String, Fahrschueler>();
	}

	public Systemereignis theoriestundeEintragen(int thema, Date beginn)
	{
		Systemereignis systemereignis = null;
		boolean raumFrei = true;
		boolean lehrerinVerfuegbar = false;
		
		for (Theoriestunde theorie : theoriestunden)
		{
			Calendar endZeitpunkt = Calendar.getInstance();
			endZeitpunkt.setTime(theorie.getBeginn());
			endZeitpunkt.add(Calendar.MINUTE, 90);
			Calendar endZeitpunktNeu = Calendar.getInstance();
			endZeitpunktNeu.setTime(beginn);
			endZeitpunktNeu.add(Calendar.MINUTE, 90);
			Date anfangszeitpunktDate = theorie.getBeginn();
			Date endzeitpunktDate = endZeitpunkt.getTime();
			Date anfangszeitpunktNeuDate = beginn;
			Date endzeitpunktNeuDate = endZeitpunktNeu.getTime();
			boolean endetDavor = endzeitpunktDate
					.before(anfangszeitpunktNeuDate);
			boolean beginntDanach = anfangszeitpunktDate
					.after(endzeitpunktNeuDate);
			raumFrei &= endetDavor || beginntDanach;
			if (!raumFrei) {
				systemereignis = new Systemereignis(
						Nachricht.Theoriestunde_nicht_hinzugefuegt_Raum_belegt);
				break;
			}
		}
		
		if (raumFrei)
		{
			for (Fahrlehrerindaten eineLehrerin : fahrlehrerinnen)
			{
				if (eineLehrerin.istFrei(beginn, 90))
				{
					Theoriestunde neueTheoriestunde = new Theoriestunde(thema, beginn);
					theoriestunden.add(neueTheoriestunde);
					eineLehrerin.addTheoriestunde(neueTheoriestunde);
					systemereignis = new Systemereignis(Nachricht.Theoriestunde_erfolgreich_hinzugefuegt);
					lehrerinVerfuegbar = true;
					break;
				}
			}
			if (!lehrerinVerfuegbar) {
				systemereignis = new Systemereignis(
						Nachricht.Theoriestunde_nicht_hinzugefuegt_keine_Lehrerin_verfuegbar);
			}
		}
		return systemereignis;
	}

	public static Unterrichtsverwaltung getInstance() {
		if (instance == null) {
			instance = new Unterrichtsverwaltung();
		}
		return instance;
	}

	
	public String unterrichtsstundenZurueckgeben()
	{
		String unterrichtsstunden = "Keine Unterrichtsstunden im System vorhanden";
		if (!fahrlehrerinnen.isEmpty() && (!fahrstunden.isEmpty() || !theoriestunden.isEmpty()))
		{
			unterrichtsstunden = "";
			for (Fahrlehrerindaten lehrerin : fahrlehrerinnen)
			{
				unterrichtsstunden += lehrerin.toString() + "\n";
			}
		}
		return unterrichtsstunden;
	}

	public boolean addFahrlehrerin(Fahrlehrerindaten fahrlehrerin) {
		return fahrlehrerinnen.add(fahrlehrerin);
	}

	public Fahrschueler addFahrschueler(Fahrschueler neuerFahrschueler) {
		return fahrschueler.put(neuerFahrschueler.getID(), neuerFahrschueler);
	}

	public void reset() {
		instance = null;
	}

	
	
	/////////////////////////////////////////////////////////////////////////////////////////
	public Systemereignis theoriestundeVermerken(String schuelerID, Date beginn)
	{
		Systemereignis systemereignis = null;
		boolean bereitsBesucht = false;
		boolean grundlageUndBereits12Grundlagen = false;
		boolean sonderUndBereitsBesucht = false;
		Fahrschueler schueler = fahrschueler.get(schuelerID);
		if (schueler == null) {
			System.err.println("PRE: schueler existiert");
		}
		Theoriestunde stunde = null;
		for (Theoriestunde eineStunde : theoriestunden)
		{
			if (eineStunde.getBeginn().compareTo(beginn) == 0)
			{
				stunde = eineStunde;
				break;
			}
		}
		if (stunde == null)
		{
			System.err.println("PRE: stunde existier");
		}
		
		bereitsBesucht = schueler.hatTheoriestundeBereitsBesucht(stunde);
		
		if (!bereitsBesucht)
		{
			if (stunde.istGrundlagenstunde())
			{
				if (schueler.hatZwoelfGrundlagen())
				{
					grundlageUndBereits12Grundlagen = true;
					systemereignis = new Systemereignis(Nachricht.Theoriestunde_nicht_vermerkt_bereits_genug_Grundlagen);
				}
			} 
			else
			{
				if (schueler.hatSonderthemaBereits(stunde.getThema()))
				{
					sonderUndBereitsBesucht = true;
					systemereignis = new Systemereignis(Nachricht.Theoriestunde_nicht_vermerkt_bereits_Sonderthema);
				}
			}
			if (!grundlageUndBereits12Grundlagen && !sonderUndBereitsBesucht)
			{
				schueler.vermerkeTheoriestunde(stunde);
				systemereignis = new Systemereignis(Nachricht.Theoriestunde_erfolgreich_vermerkt);
			}
		}
		else
		{
			systemereignis = new Systemereignis(Nachricht.Theoriestunde_nicht_vermerkt_bereits_vermerkt);
		}
		return systemereignis;
	}
	/////////////////////////////////////////////////////////////////////////////////////////////////////////
}
