package drivenow.boundaries;

import drivenow.controls.Unterrichtsverwaltung;
import drivenow.interfaces.IFahrlehrerinBoundary;

public class FahrlehrerinBoundary implements IFahrlehrerinBoundary
{

	private Unterrichtsverwaltung unterrichtsverwaltung;

	public FahrlehrerinBoundary()
	{
		unterrichtsverwaltung = Unterrichtsverwaltung.getInstance();
	}

	@Override
	public String unterrichtsstundenZurueckgeben()
	{
		return unterrichtsverwaltung.unterrichtsstundenZurueckgeben();
	}

}
