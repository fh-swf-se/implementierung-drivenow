package drivenow.boundaries;

import java.util.Date;

import drivenow.Systemereignis;
import drivenow.controls.Ressourcenverwaltung;
import drivenow.controls.Unterrichtsverwaltung;
import drivenow.interfaces.ISekretaerBoundary;

public class SekretaerBoundary implements ISekretaerBoundary
{
	private Ressourcenverwaltung ressourcenverwaltung;
	private Unterrichtsverwaltung unterrichtsverwaltung;

	public SekretaerBoundary()
	{
		ressourcenverwaltung = Ressourcenverwaltung.getInstance();
		unterrichtsverwaltung = Unterrichtsverwaltung.getInstance();
	}

	@Override
	public Systemereignis fahrlehrerinEintragen(String name, String kennzeichen) {
		return ressourcenverwaltung.fahrlehrerinEintragen(name, kennzeichen);
	}

	@Override
	public Systemereignis fahrschuelerEintragen(String name, String anschrift)
	{
		return ressourcenverwaltung.fahrschuelerEintragen(name, anschrift);
	}

	@Override
	public Systemereignis fahrschulautoEintragen(String kennzeichen) {
		return ressourcenverwaltung.fahrschulautoEintragen(kennzeichen);
	}

	@Override
	public Systemereignis theoriestundeEintragen(int thema, Date beginn) {
		return unterrichtsverwaltung.theoriestundeEintragen(thema, beginn);
	}


	////////////////////////////////////////////////////////////////////////////////////////
	@Override
	public Systemereignis theoriestundeVermerken(String schuelerID, Date beginn)
	{
		return unterrichtsverwaltung.theoriestundeVermerken(schuelerID, beginn);
	}
	////////////////////////////////////////////////////////////////////////////////////////



	@Override
	public String datenbestandZurueckgeben() {
		return ressourcenverwaltung.datenbestandZurueckgeben();
	}

}
